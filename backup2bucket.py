#!/usr/bin/python3

import os
import sys
import pexpect as px
from datetime import date as date

script_help = """\nScript para realizar backup criptografado do banco,
        - filtra e seleciona apenas o backup mais recente
        - criptografa e comprime usando 7z
        - envia para armazenamento de objetos usando s3cmd
            "s3cmd put backup.sql s3://mybucket/backup.sql"
    Args:
        - senha de cripto para o backup que será enviado
        - caminho para os backups
        - endereço do bucket

    Exemplo:
        ./backup2bucket.py cHaJeBHg7LEdn /backups s3://mybucket/backup.sql.7z
"""


def getmtime(entry):
    '''retorna data de criação dos arquivos'''
    return entry.stat().st_mtime


if len(sys.argv) < 4:
    print(script_help)
    sys.exit()

# Inicializa as variáveis
date = date.today().strftime('%Y-%m-%d')
pwd = sys.argv[1]
backups_path = sys.argv[2]
bucket = sys.argv[3]
print(pwd, backups_path, bucket)

dir_entries = sorted(os.scandir(backups_path), key=getmtime, reverse=True)[0]
newer_backup = dir_entries.path
base = os.path.basename(newer_backup)
clear_path = newer_backup.replace(base, '')
encrypted_file = f"{clear_path}{date}-{base}.7z"
child = px.spawn(f'7z a {encrypted_file} {newer_backup} -p')
try:
    child.expect('.* password .*')
    child.sendline(pwd)
    child.close()
except px.exceptions.TIMEOUT:
    print("Exception was thrown")
    print("debug information:")
    print(str(child))
    child.close()
