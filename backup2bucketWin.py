#!/usr/bin/python3

import os
import sys
import subprocess as sb
from datetime import date as dt

HELP = """\nScript para realizar backup criptografado do banco de dados,
       - filtra e seleciona apenas o backup mais recente
       - criptografa e comprime usando 7z
       - envia para armazenamento de objetos usando s3cmd
   Args:
       - Senha para criptografar o backup antes do envio
       - Caminho para os backups (com delimitadores de diretórios escapados)
       - Endereço do bucket (sem incluir o protocolo ou barras iniciais)
"""


def getmtime(entry):
    """retorna data de criação dos arquivos"""
    return entry.stat().st_mtime


if len(sys.argv) < 4:
    print(HELP)
    sys.exit()

# Inicializa constantes
PWD = sys.argv[1]
BACKUP_PATH = sys.argv[2]
BUCKET = "s3://" + sys.argv[3]
DATE = dt.today().strftime('%Y-%m-%d')

# Seleciona backup mais recente da pasta de backups, comprime e criptografa
backup_files = sorted(os.scandir(BACKUP_PATH), key=getmtime, reverse=True)[0]
newest_backup = backup_files.path
basename = os.path.basename(newest_backup)
clean_path = newest_backup.replace(basename, '')
zipped = f"{clean_path}{DATE}-{basename}.7z"
cmd = ["7z.exe", "a", zipped, f"-p{PWD}", "-y", newest_backup]
try:
    sb.check_call(cmd, executable="C:\\Program Files\\7-Zip\\7z.exe",
                    stderr=sb.STDOUT, stdout=sb.PIPE)

    # Envia backup criptografado para o bucket
    S3CMD_PATH = "C:\\Program Files (x86)\\s3cmd-2.1.0\\s3cmd"
    bkt_cmd = ["python", S3CMD_PATH, 'put', zipped, BUCKET]
    sb.run(bkt_cmd, shell=True)
except:
    print('exception')

# Remove backups mais antigos que 3 dias
for folder, subs, files in os.walk(BACKUP_PATH):
    for filename in files:
        file_path = os.path.abspath(os.path.join(folder, filename))
        if os.path.getmtime(file_path) > 86400 * 3:
            os.remove(file_path)
