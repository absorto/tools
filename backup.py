#!/usr/bin/python3
import os
import sys
from datetime import date
import subprocess as sb
import pexpect

"""
    Script to be ran from CRON to backup the database and filesystem
        - both are sent to a Linode bucket
        - database backups are sent nightly
        - filesystem is sent once a week
    Required args: password to encrypt backup, database name and bucket address
"""


def open_expect_process(cmd, pattern, userinput):
    child = pexpect.spawn(cmd)
    try:
        child.expect(pattern)
        child.sendline(userinput)
        child.close()
    except pexpect.exceptions.TIMEOUT:
        print('pexpect timed out')
        child.close()


def send_to_bucket(file, bucket):
    sb.run(f'linode-cli obj put {file} {bucket}')


# Inicializa credenciais e outras variáveis necessárias
date = date.today().strftime('%Y-%m-%d')
pwd = sys.argv[1]
db = sys.argv[2]
bucket = sys.argv[3]
PATTERN = '.* password .*'

# Backup da base de dados
db_file = f'/backup/absorto-wiki-{date}.sql.7z'
sb.run(f'mysqldump -u root {db} > {db_file}')
open_expect_process(f'7z a {db_file} -p', PATTERN, pwd)
send_to_bucket(db_file, bucket)

# Backup do diretório
if date.today().strftime('%a') == 'Sat':
    archive = f'/backup/{date}-absorto-wiki.tar.7z'
    sb.run(f'tar cf {archive} -X /var/www/absortowiki/backup-exclusions \
           /var/www/absortowiki')
    open_expect_process(f'7z a -si {archive} -p', PATTERN, pwd)
    send_to_bucket(archive, bucket)

    # Remove backup files older than two weeks from dir
    for folder, subs, files in os.walk('/backup'):
        for filename in files:
            file_path = os.path.abspath(os.path.join(folder, filename))
            if os.path.getmtime(file_path) > 1209600:
                os.remove(file_path)
