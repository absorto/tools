#!/bin/bash

echo Adding Symlinks...
dotfilesDir="${HOME}/dotfiles"
ln -s ${dotfilesDir}/lxterminal.conf ${HOME}/.config/lxterminal/lxterminal.conf
ln -s ${dotfilesDir}/i3/config ${HOME}/.config/i3/config
ln -s ${dotfilesDir}/i3/i3blocks.conf ${HOME}/.config/i3/i3blocks.conf
ln -s ${dotfilesDir}/.tmux.conf ${HOME}/.tmux.conf
ln -s ${dotfilesDir}/moc/keymap ${HOME}/.moc/keymap
ln -s ${dotfilesDir}/moc/config ${HOME}/.moc/config
