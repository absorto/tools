#!/bin/bash

#TODO: Implement non-repo software installation
#TODO: Add sys.argv options for local server env install
#TODO: chmod/chown everything
#TODO: symlinking not working properly
#TODO: theming, flat-remix-blue-dark and gsettings / gtk2.0 / gtk3.0/
#TODO: think about reducing verbosity

read -n 1 -p "Criou o user e adicionou ao grupo sudo? S/N" input
if [ "$input" = "N" ]; then
	exit
fi

./apt.sh

echo 'Cloning dotfiles...'
cd ${HOME}
git clone https://gitlab.com/absorto/dotfiles.git dotfiles
./symlink.sh

cd dotfiles/programs/
./pkgdownload.sh
./signaldesktop.sh
./i3gaps.sh
./vim.sh

# Replace system wide crontab with my tabs
cp /home/absorto/dotfiles/crontab_backup /etc/crontab

# Keyboard l18n
#cp /home/absorto/dotfiles/keyboard /etc/default/keyboard # Change keyboard config to Thinkpad layout
#setxkbmap -layout "br" -model "IBM ThinkPad R60/T60/R61/T61" # a4techKB21

# load bashrc
source ~/.bashrc

# And we are finished
figlet "...estamos de volta" | lolcat
