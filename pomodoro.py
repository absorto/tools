#! /usr/bin/python3
from time import sleep
from datetime import timedelta
import subprocess as sb


def notify(msg):
    sb.run(f'notify-send \"{msg}\"', shell=True)


def pomodoro_timer(num):
    '''25 minutes focus streak'''
    notify(f"Pomodoro {num} started!")
    sleep(1500)
    notify("Break")


def break_timer(time):
    '''start break defined by time in seconds'''
    notify(f"Starting {round(time/60)} interval")
    sleep(time)
    notify("Back to work")


'''
 start pomodoro by typing go
 start break right after the pomodoro timer finishes
 start another pomodoro by typing 'go' once more
 quit at any time by typing 'q'
    TODO: implement ongoing display
'''
count = 1
SHORT = 300
LONG = 900
run = None
while run is None:
    run = input("Type \'go\' to start a pomodoro timer\n")
    while run == 'go' and run != 'q':
        #print(f"Starting pomodoro {count}")
        run = None
        pomodoro_timer(count)
        break_timer(LONG) if count % 4 == 0 else break_timer(SHORT)
        count += 1
