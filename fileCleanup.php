<?php
/**
 * Remove files older than given time in a given directory
 * ARGS are:
 * - full path for directory
 * - maxtime files must be available in seconds
 * - filetype
 * Example: fileCleanup.php /home/user/Documents/ 60 zip
 */
if ( $argc == 4 ) {
	$path = $argv[1];
	$maxtime = $argv[2];
	$exclude = "*." . $argv[3];
	$now = time();
} else {
	die (
		"\tRemove files older than <maxtime> in a given directory.\n
		Usage:
		fileCleanup.php <path> <maxtime> <filetype>\n
		Example:
		fileCleanup.php /home/user/Documents/ 3600 zip\n"
	);
}

// Create a list from all files matching the exclude pattern
$files = glob( $path . $exclude );

// Get file modification time
foreach ( $files as $filename ) {
	print "checking $filename creation time...\n";
	$creationTime = filectime( $filename );
	if ( $creationTime + $maxtime <= $now ) {
		exec( "rm '$filename'" );
		print "Removing $filename created in  "
			. date( "F d Y H:i:s.", $creationTime )
			. "\n";
	}
}

