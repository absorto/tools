<?php
/**
 * As of version 1.35, by default, passwords stored in the user table are formed
 * from the concatenation of the following strings delimited by a collon:
 * pbkdf2 => representing the function used to process the cipher
 * sha512 => representing the hash used in the function
 * 30000  +> representind the cost used in the function
 * 64 => reprensenting the desired length of the resulting hash
 * VEw39AjQhysE+7BWFPjpcg== => a randonmly generated salt
 * IUsmV5o6e+/w199EInFzM60WUkmlZE5gL0W/Ue/PnrxZXYo0aE5X/io6T5Jwp7WsyCr+fXm64Qm8tq6vyuvvoQ== => the hash result
 * --
 * The password will be echoed and can then be added to a user directly in SQL with the following statement
 * UPDATE prefix_user SET user_password="<echoed_pwd>" WHERE user_id=1;
 */

// Get information from args
$pwd = $argv[1];
if ( strlen( $pwd ) < 10 ) {
	exit( "Minimal password length is 10\n" );
}

$salt = base64_encode( openssl_random_pseudo_bytes( 16 ) );
$hash = hash_pbkdf2( 'sha512', $pwd, $salt, 3000, 64 );
$hash = base64_encode( $hash );
echo ":pbkdf2:sha512:30000:64:$salt:$hash\n";
