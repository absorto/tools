#!/usr/bin/python3
# -*- coding:utf-8 -*-

import sys
import json
import urllib.request as urllib2


def getCotacao(currency):
    """retorna a cotação atual da currency recebida"""
    url = f'https://economia.awesomeapi.com.br/all/{currency}-BRL'
    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;\
                    q=0.9,image/webp,*/*;q=0.8',
               'Accept-Encoding': 'none',
               'Accept-Language': 'en-US,en;q=0.5',
               'Connection':  'keep-alive',
               'User-Agent':  'Mozilla/5.0 (X11; Linux x86_64; rv:74.0)\
                    Gecko/20100101 Firefox/74.0'
               }
    request = urllib2.Request(url, headers=headers)
    data = json.load(urllib2.urlopen(request))
    return float(data[currency]['low'])


def getConfig(config_file, company):
    """retorna charge per unit e currency do config_file e company recebidos"""
    with open(config_file) as c:
        config = json.load(c)
        cpu = float(config[company]["cpu"])
        currency = config[company]["currency"]
    return (cpu, currency)


try:
    total_hours = float(sys.argv[1])
    config_file = sys.argv[2]
    company = sys.argv[3]
except ValueError:
    total_hours = float(sys.argv[1].replace(':', '.'))
    config_file = sys.argv[2]
    company = sys.argv[3]
except IndexError:
    print("Too few arguments provided")
    print("Usage:\n hours config_file_path company\n")
    sys.exit()

cpu, currency = getConfig(config_file, company)
if currency != 'BRL':
    cotacao = getCotacao(currency)
    total = total_hours * cpu
    brl_total = total * cotacao
    print(f'Cotação: R${cotacao:.2f}\n{currency}{total:.2f}\nR${brl_total:.2f}')
else:
    brl_total = total_hours * cpu
    print(f'R$: {brl_total:.2f}')


