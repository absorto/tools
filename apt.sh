#!/bin/bash

# Adds non-free repositories to deb sources list
(
echo deb http://debian.c3sl.ufpr.br/debian/ buster main non-free
echo deb-src http://debian.c3sl.ufpr.br/debian/ buster main non-free
echo deb http://security.debian.org/debian-security buster/updates main contrib non-free
echo deb-src http://security.debian.org/debian-security buster/updates main contrib non-free
echo deb http://debian.c3sl.ufpr.br/debian/ buster-updates main contrib non-free
echo deb-src http://debian.c3sl.ufpr.br/debian/ buster-updates main contrib non-free
) > /etc/apt/sources.list

sudo apt update

# Install Debian native packages
function install {
	which $1 &> /dev/null

	if [ $? -ne 0 ]; then
		echo "Installing: ${1}..."
		sudo apt install -y $1
	else
		echo "${1} is already installed"
	fi
}

install keepassxc
install xinit
install i3
install lxterminal
install git
install firmware-iwlwifi
install neofetch
install moc
install thunar
install taskwarrior
install calcurse
install thunderbird
install rsync
install curl
install pulseaudio
install pavucontrol
install compton
install lm-sensors
install htop
install feh
install universal-ctags
install fonts-font-awesome
install fonts-noto-cjk
install scrot
install youtube-dl
install lolcat
install texlive-base
install texlive-latex-recommended
install texlive-fonts-recommended
install texlive-latex-extra
install pandoc
install wicd
install figlet
install gcc
install make
install dh-autoreconf
install libxcb-keysyms1-dev
install libpango1.0-dev
install libxcb-util0-dev
install xcb
install libxcb1-dev
install libxcb-icccm4-dev
install vlc
install bleachbit
install arandr
install tmux
install libyajl-dev
install libev-dev
install libxcb-xkb-dev
install libxcb-cursor-dev
install libxkbcommon-dev
install libxcb-xinerama0-dev
install libxkbcommon-x11-dev
install libstartup-notification0-dev
install libxcb-randr0-dev libxcb-xrm0
install libxcb-xrm-dev
install libxcb-shape0-dev
install libncurses-dev
install python3-dev
