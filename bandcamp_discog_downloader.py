#!/usr/bin/python3
import os
import sys

filename = sys.argv[1]
with open(filename, 'r') as f:
    counter = 0
    lines = f.readlines()
    albums = len(lines)
    for url in lines:
        url = url.strip()
        album = url.split("/")[-1]
        print(f"++++ Starting download of {album}. {counter} of {albums} ++++")
        os.system(f"bandcamp-dl {url}")
        counter += 1
