#!/usr/bin/python3

from sys import argv, exit
import subprocess as sb

"""
Automate adding users from a batch file to your Prosody server using prosodyctl
- the file must be passed as the first ARG with full path
- expected file format is a CSV with one user per line containing:
    username, domain, password
"""

error = "Error: a CSV file was expected as the only arg"
accounts_file = open(argv[1], 'r') if len(argv) == 2 else exit(error)
line = accounts_file.readline()
while line:

    # Process line and run command
    user, domain, pwd = line.split(',')
    cmd = ['prosodyctl', 'register']
    cmd.append(user.strip())
    cmd.append(domain.strip())
    cmd.append(pwd.strip())
    sb.run(cmd)
    print(f"adding {user} in {domain}...")

    # Read next line
    line = accounts_file.readline()

accounts_file.close()
