#!/usr/bin/python3
import os
import sys
import subprocess as sb
from datetime import date


def compress(filename, pwd):
    zipped = f"{filename}.7z"
    cmd = f"7z a {zipped} {filename} -p"
    pattern = ".* password .*"
    open_expect_process(cmd, pattern, pwd)
    return zipped


def open_expect_process(cmd, pattern, userinput):
    import pexpect
    child = pexpect.spawn(cmd)
    try:
        child.expect(pattern)
        child.sendline(userinput)
        child.close()
    except pexpect.exceptions.TIMEOUT:
        print("Received TIMEOUT from pexpect")
        child.close()


def send_to_bucket(archive, bucket):
    sb.run(["linode-cli", "obj", "put", archive, bucket])


def database_backup(db, db_archive):
    os.system(f"mysqldump -u root {db} > {db_archive}")


def file_system_backup(fs_archive, exclusions_path, target_path):
    sb.run(['tar', 'cf', fs_archive, '-X', exclusions_path, target_path])


def remove_old():
    """Remove backup files older than two weeks from dir"""
    for folder, subs, files in os.walk('/backup'):
        for filename in files:
            file_path = os.path.abspath(os.path.join(folder, filename))
            if os.path.getmtime(file_path) > 1209600:
                os.remove(file_path)


# Initialize variables
try:
    _, pwd, db, bucket, exclusions, wiki_path = sys.argv
except ValueError:
    print("""
        Script to be ran from CRON to backup the database and filesystem
            - both are sent to a Linode bucket
            - database backups are sent nightly
            - filesystem is sent once a week
        Required args:
            password to encrypt backup
            database name
            bucket address
            full path for exclusions list file
            full path for the wiki install for file system backups
        Usage example:
          bucket_backup.py kaur3huaf1?q/90a wiki \
name.region-re-1.linodeobjects.com /var/www/backup-exclusions /var/www/wiki
        """)
    sys.exit()

today = date.today().strftime("%Y-%m-%d")
if not os.path.exists("/backup"):
    os.makedirs("/backup")
fs_archive = f"/backup/{os.uname().nodename}-backup-{today}.tar"
db_archive = f"/backup/{db}-backup-{today}.sql"

# Database Backup
database_backup(db, db_archive)
db_compressed = compress(db_archive, pwd)
send_to_bucket(db_compressed, bucket)

# Process file system backups
if date.today().strftime("%a") == "Sat":
    file_system_backup(fs_archive, exclusions, wiki_path)
    fs_compressed = compress(fs_archive, pwd)
    send_to_bucket(fs_compressed, bucket)
    remove_old()
