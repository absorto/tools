#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess as sb
import os

# TODO: Implement software installation
# TODO: Add sys.argv options for local server env install
# TODO: chmod/chown everything
# TODO: symlinking not working properly
# TODO: theming, flat-remix-blue-dark and gsettings / gtk2.0 / gtk3.0/
# TODO: think about reducing verbosity


def find(pattern, path):
    import fnmatch
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                return os.path.join(root, name)


def symlinking(configPath, defaultPath):
    if not os.path.exists(defaultPath):
        os.makedirs(defaultPath)
    os.chdir(defaultPath)
    pkg = configPath.split('/')[-1].split('.')[0]
    confName = find(pkg + '*', '/home/absorto/dotfiles').split('/')[-1]
    sb.run(['ln', '-sf', configPath, confName])
    return "creating link from {} for {} in {}".format(
        configPath, confName, defaultPath)


# sb.run('sudo adduser absorto sudo', shell=True) Deprecated on newer debian
sb.run('sudo usermod -aG sudo absorto', shell=True)

# Replace original sources.list with one that adds non-free repositories
sb.run('echo "deb http://debian.c3sl.ufpr.br/debian/ buster main non-free\n\
    deb-src http://debian.c3sl.ufpr.br/debian/ buster main non-free\n\
    deb http://security.debian.org/debian-security buster/updates main contrib non-free\n\
    deb-src http://security.debian.org/debian-security buster/updates main contrib non-free\n\
    deb http://debian.c3sl.ufpr.br/debian/ buster-updates main contrib non-free\n\
    deb-src http://debian.c3sl.ufpr.br/debian/ buster-updates main contrib non-free\n" \
    > /etc/apt/sources.list', shell=True)

# Update and upgrade system
sb.run('apt update && apt upgrade -y', shell=True)

# Install Debian native packages
pkg = ['xinit', 'i3', 'lxterminal', 'git', 'firmware-iwlwifi', 'neofetch',
        'moc', 'thunar', 'keepassxc', 'taskwarrior', 'calcurse', 'thunderbird',
        'rsync', 'curl', 'pulseaudio', 'pavucontrol', 'compton', 'lm-sensors',
        'htop', 'feh', 'universal-ctags', 'fonts-font-awesome', 'fonts-noto-cjk',
        'scrot', 'youtube-dl', 'lolcat', 'texlive-base', 'texlive-latex-recommended',
        'texlive-fonts-recommended', 'texlive-latex-extra', 'pandoc', 'wicd',
        'figlet', 'gcc', 'make', 'dh-autoreconf', 'libxcb-keysyms1-dev',
        'libpango1.0-dev', 'libxcb-util0-dev', 'xcb', 'libxcb1-dev',
        'libxcb-icccm4-dev', 'vlc', 'bleachbit', 'arandr', 'tmux', 'libyajl-dev',
        'libev-dev', 'libxcb-xkb-dev', 'libxcb-cursor-dev', 'libxkbcommon-dev',
        'libxcb-xinerama0-dev', 'libxkbcommon-x11-dev', 'libstartup-notification0-dev',
        'libxcb-randr0-dev', 'libxcb-xrm0', 'libxcb-xrm-dev', 'libxcb-shape0-dev',
        'libncurses-dev', 'python3-dev', 'python3-pip', 'ranger', 'fonts-inconsolata',
        'ripgrep'
         ]
pkgInstall = sb.run(['apt', 'install', '-y', pkg])

if pkgInstall.returncode == 0:
    print('Debian native packages installed...')
else:
    print("Debian native package install failed with {} return code".format(
        pkgInstall.returncode))

# User must be added here
print('Cloning dotfiles...')
os.chdir('/home/absorto')
sb.run(['git', 'clone', 'https://gitlab.com/absorto/dotfiles.git'])

# Symlinks from dotfiles to their places
print('Adding Symlinks...')
symlinking('/home/absorto/dotfiles/lxterminal.conf', '/home/absorto/.config/lxterminal/')
symlinking('/home/absorto/dotfiles/i3/config',  '/home/absorto/.config/i3/')
symlinking('/home/absorto/dotfiles/i3/i3blocks.conf',  '/home/absorto/.config/i3/')
symlinking('/home/absorto/dotfiles/.tmux.conf', '/home/absorto')
symlinking('/home/absorto/dotfiles/moc/keymap', '/home/absorto/.moc/')
symlinking('/home/absorto/dotfiles/moc/config', '/home/absorto/.moc/')

# Download packages from other sources
print('Downloading software not available in the Debian repos...')
os.mkdir('/home/absorto/opt')
os.chdir('/home/absorto/opt')
# sb.run(['wget', 'https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-US']) # Firefox Latest
# add tor, exodus
sb.run(['wget', 'https://telegram.org/dl/desktop/linux'])  # Telegram

# Installing downloaded software
print('Installing software not available in the Debian repos... NOT IMPLEMENTED YET')

# Add Signal signing keys and official repo to sources and proceed with installation
sb.run('curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -', shell=True)
sb.run('echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list', shell=True)
sb.run('apt update && apt install -y signal-desktop', shell=True)

# Clone and compile i3-gaps
os.chdir('/home/absorto/')
sb.run('git clone https://github.com/Airblader/i3 i3-gaps', shell=True)
os.chdir('/home/absorto/i3-gaps')
sb.run('autoreconf --force --install', shell=True)
sb.run('rm -rf build/', shell=True)
os.mkdir('build')
os.chdir('build/')
sb.run('../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers', shell=True)
sb.run('make')
sb.run('make install', shell=True)

# Compile Vim with Python3 support
# ./configure --with-features=huge \
#         --enable-multibyte \
#         --enable-python3interp=yes \
#         --with-python3-config-dir=/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu \
#         --enable-perlinterp=yes \
#         --enable-cscope \
#         --prefix=/usr/local

# make VIMRUNTIMEDIR=/usr/local/share/vim/vim81
# cd ~/vim
# make install
# update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
# update-alternatives --set editor /usr/local/bin/vim
# update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
# update-alternatives --set vi /usr/local/bin/vim

# check if vim --version outputs +python3

# Configuration
sb.run('cp /home/absorto/dotfiles/crontab_backup /etc/crontab', shell=True)  # Replace system wide crontab with my tabs

# Keyboard localization
sb.run('cp /home/absorto/dotfiles/keyboard /etc/default/keyboard', shell=True)  # Change keyboard config to Thinkpad layout
sb.run('setxkbmap -layout "br" -model "IBM ThinkPad R60/T60/R61/T61"', shell=True)  # a4techKB21

# And we are finished
sb.run('figlet "...estamos de volta"', shell=True)
