#!/bin/bash

echo Cloning i3-gaps...
# Clone and compile i3-gaps
cd ${HOME}
git clone https://github.com/Airblader/i3 i3-gaps
cd ${HOME}/i3-gaps
autoreconf --force --install
rm -rf build/
mkdir build && cd build/
../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
make
make install
