#!/bin/bash

echo Compiling Vim with Python3 support...

# Remove vim if installed
sudo apt remove -y vim vim-runtime gvim

# Clone vim repo
cd ~
git clone https://github.com/vim/vim.git

# Compile Vim with Python3 support
./configure --with-features=huge \
	--enable-multibyte \
	--enable-python3interp=yes \
	--with-python3-config-dir=/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu \
	--enable-perlinterp=yes \
	--enable-cscope \
	--prefix=/usr/local

make VIMRUNTIMEDIR=/usr/local/share/vim/vim81
cd ~/vim
make install
update-alternatives --install /usr/bin/editor editor /usr/local/bin/vim 1
update-alternatives --set editor /usr/local/bin/vim
update-alternatives --install /usr/bin/vi vi /usr/local/bin/vim 1
update-alternatives --set vi /usr/local/bin/vim

# check if vim compilation with py3 worked
result=$(vim --version|grep -w '+python3')
if [ $result ]
then
	echo 'Vim compiled with Python3 support correctly'
else
	echo 'Vim compilation failed'
fi
