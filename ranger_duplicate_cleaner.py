#!/usr/bin/python3

import os
import re
import sys
import shutil


usage = """
        Error: Missing path to clean in passed args
            ranger_duplicate_cleaner.py /home/user/Documents
        """

directory = sys.argv[1] if len(sys.argv) >= 2 else sys.exit(usage)
for root, subdir, files in os.walk(directory):
    for sub in subdir:
        if re.match(r'.+?_$', sub):
            path = os.path.join(root, sub)
            print(f"removing... {path}\n")
            try:
                shutil.rmtree(path)
            except OSError as e:
                print(f"Error: {path} : {e.strerror}")
        else:
            print(f"no file matching pattern found in {sub}")
